package AddOn;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateFormatterToAlphabet {
    public String DateFormatterToAlphabet(String date) {
        SimpleDateFormat inputFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
        try {
            Date dateObj = inputFormat.parse(date);
            return outputFormat.format(dateObj);
        } catch (ParseException e) {
            e.printStackTrace();
            return ""; // Tangani kesalahan jika format tanggal tidak sesuai
        }
    }
}
