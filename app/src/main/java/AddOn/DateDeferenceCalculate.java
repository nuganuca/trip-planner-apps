package AddOn;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateDeferenceCalculate {
    private long calculateDateDifference(String startDate, String endDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());

        try {
            Date startDateObj = dateFormat.parse(startDate);
            Date endDateObj = dateFormat.parse(endDate);

            // Hitung selisih dalam milidetik
            long differenceInMillis = endDateObj.getTime() - startDateObj.getTime();

            // Konversi selisih milidetik ke hari
            long differenceInDays = differenceInMillis / (24 * 60 * 60 * 1000);

            return differenceInDays;
        } catch (ParseException e) {
            e.printStackTrace();
            return 0; // Tangani kesalahan jika format tanggal tidak sesuai
        }
    }
}
