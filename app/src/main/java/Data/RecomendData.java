package Data;

import com.example.vehicletrackingapp.Recomend;

public class RecomendData {
    private String address_label;
    private String imgUrl;
    private double lat;
    private double lang;
    private String title;
    public RecomendData(){

    }
    public RecomendData(String address_label, String imgUrl, double lat, double lang, String title) {
        this.address_label = address_label;
        this.imgUrl = imgUrl;
        this.lat = lat;
        this.lang = lang;
        this.title = title;
    }

    public String getAddress_label() {
        return address_label;
    }

    public void setAddress_label(String address_label) {
        this.address_label = address_label;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLang() {
        return lang;
    }

    public void setLang(double lang) {
        this.lang = lang;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
