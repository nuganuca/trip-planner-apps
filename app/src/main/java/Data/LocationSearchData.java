package Data;

public class LocationSearchData {
    private String title;
    private String addressLabel;
    private double latitude;
    private double longitude;

    public LocationSearchData(String title, String addressLabel, double latitude, double longitude) {
        this.title = title;
        this.addressLabel = addressLabel;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getTitle() {
        return title;
    }

    public String getAddressLabel() {
        return addressLabel;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}

