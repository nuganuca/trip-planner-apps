package Data;

import java.util.Date;

public class TripModel {
    private String title;
    private String todo;
    private String address_label;
    private double lat;
    private double lang;
    private String date;
    private String second_date;
    private boolean saved;
    private boolean visited;
    boolean alarm;
    private Date uploaded;
    private String imgUrl;
    private String title_lowercase;
    public TripModel(){

    }
    public TripModel(String title, String todo, String address_label, double lat, double lang, String date, String second_date, boolean saved, boolean visited, Date uploaded, String imgUrl, String title_lowercase, boolean alarm) {
        this.title = title;
        this.todo = todo;
        this.address_label = address_label;
        this.lat = lat;
        this.lang = lang;
        this.date = date;
        this.second_date = second_date;
        this.saved = saved;
        this.visited = visited;
        this.uploaded = uploaded;
        this.imgUrl = imgUrl;
        this.title_lowercase = title_lowercase;
        this.alarm = alarm;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTodo() {
        return todo;
    }

    public void setTodo(String todo) {
        this.todo = todo;
    }

    public String getAddress_label() {
        return address_label;
    }

    public Date getUploaded() {
        return uploaded;
    }

    public void setUploaded(Date uploaded) {
        this.uploaded = uploaded;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getTitle_lowercase() {
        return title_lowercase;
    }

    public void setTitle_lowercase(String title_lowercase) {
        this.title_lowercase = title_lowercase;
    }

    public void setAddress_label(String address_label) {
        this.address_label = address_label;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLang() {
        return lang;
    }

    public void setLang(double lang) {
        this.lang = lang;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSecond_date() {
        return second_date;
    }

    public void setSecond_date(String second_date) {
        this.second_date = second_date;
    }

    public boolean isSaved() {
        return saved;
    }

    public void setSaved(boolean saved) {
        this.saved = saved;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public boolean isAlarm() {
        return alarm;
    }

    public void setAlarm(boolean alarm) {
        this.alarm = alarm;
    }
}
