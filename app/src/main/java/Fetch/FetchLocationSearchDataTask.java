package Fetch;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import Adapter.LocationSearchAdapter;
import AddOn.ReadUrl;
import Data.LocationSearchData;

public class FetchLocationSearchDataTask extends AsyncTask<String, Void, List<LocationSearchData>> {
    private LocationSearchAdapter locationSearchAdapter;

    public FetchLocationSearchDataTask(LocationSearchAdapter adapter) {
        this.locationSearchAdapter = adapter;
    }

    @Override
    protected List<LocationSearchData> doInBackground(String... urls) {
        if (urls.length < 1 || urls[0] == null) {
            return null;
        }

        String jsonResponse = new ReadUrl().ReadUrl(urls[0]);

        List<LocationSearchData> locationList = new ArrayList<>();

        try {
            JSONObject root = new JSONObject(jsonResponse);
            JSONArray items = root.getJSONArray("items");

            for (int i = 0; i < items.length(); i++) {
                JSONObject currentItem = items.getJSONObject(i);

                String title = currentItem.getString("title");
                String addressLabel = currentItem.getJSONObject("address").getString("label");
                double latitude = currentItem.getJSONObject("position").getDouble("lat");
                double longitude = currentItem.getJSONObject("position").getDouble("lng");

                locationList.add(new LocationSearchData(title, addressLabel, latitude, longitude));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return locationList;
    }

    @Override
    protected void onPostExecute(List<LocationSearchData> locationList) {
        if (locationList != null && !locationList.isEmpty()) {
            locationSearchAdapter.setData(locationList);
        }
    }
}
