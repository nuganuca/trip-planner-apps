package com.example.vehicletrackingapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class SplashScreen extends AppCompatActivity {
    ImageView travel;
    TextView title, desc;
    Animation topAnim, bottomAnim, fadeAnim;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        travel = findViewById(R.id.travel);
        title = findViewById(R.id.title);
        desc = findViewById(R.id.desc);
        topAnim = AnimationUtils.loadAnimation(this, R.anim.topanim);
        bottomAnim = AnimationUtils.loadAnimation(this, R.anim.bottomanim);
        fadeAnim = AnimationUtils.loadAnimation(this, R.anim.fadeanim);

        travel.setAnimation(fadeAnim);
        title.setAnimation(topAnim);
        desc.setAnimation(bottomAnim);
    }

    public void move(View view) {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}