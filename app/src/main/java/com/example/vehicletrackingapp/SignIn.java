package com.example.vehicletrackingapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import AddOn.LoadingDialog;

public class SignIn extends AppCompatActivity {
    EditText email, password;
    Button login;
    private FirebaseAuth mAuth;

    LoadingDialog loadingDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        email = findViewById(R.id.txt_email);
        password = findViewById(R.id.txt_password);
        login = findViewById(R.id.login_button);
        mAuth = FirebaseAuth.getInstance();
        loadingDialog = new LoadingDialog(this);
        TextView tvRegisterNow = findViewById(R.id.tv_register_now);
//        meisahkan kata Don't have an account? Register now
        String text = "Don't have an account? Register now";
        SpannableString spannableString = new SpannableString(text);
        ForegroundColorSpan colorSpan = new ForegroundColorSpan(getResources().getColor(R.color.primary_color));
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                startActivity(new Intent(SignIn.this, SignUp.class));
                finish();
            }
        };
        int start = text.indexOf("Register now");
        int end = start + "Register now".length();
        spannableString.setSpan(colorSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(clickableSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvRegisterNow.setText(spannableString);
        tvRegisterNow.setMovementMethod(LinkMovementMethod.getInstance());
        login.setOnClickListener(v -> {
            if ((email.getText().length() > 0) && (password.getText().length() > 0)){
                login(email.getText().toString(), password.getText().toString());
                loadingDialog.startLoadingDialog();
            }else{
                if (email.getText().length() == 0){
                    email.setError("Masukkan Email Terlebih Dahulu");
                }else if(password.getText().length() == 0){
                    password.setError("Masukkan  Password Terlebih Dahulu");
                }
            }
        });
    }
    private void login(String email, String password){
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful() && task.getResult() != null){
                    if (task.getResult().getUser() != null ){
                        reload();
                    }else{
                        Toast.makeText(SignIn.this, "Login gagal!", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(SignIn.this, "Login gagal!", Toast.LENGTH_SHORT).show();
                }
                loadingDialog.dismissDialog();
            }
        });
    }
    public void reload(){
        Intent intent = new Intent(SignIn.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}