package com.example.vehicletrackingapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.HashMap;
import java.util.Map;

import Adapter.TripAdapter;
import AddOn.DateFormatterToAlphabet;
import Data.TripModel;

public class saved_page extends AppCompatActivity {
    RecyclerView triprv;
    LinearLayoutManager linearLayoutManager;
    LinearLayout pages_null;
    DateFormatterToAlphabet dateconvert;
    FirebaseUser firebaseUser;
    FirebaseFirestore firebaseFirestore;

    TripAdapter tripAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_page);
        pages_null = findViewById(R.id.pages_null);
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        firebaseFirestore = FirebaseFirestore.getInstance();
        dateconvert = new DateFormatterToAlphabet();
        Query query = firebaseFirestore.collection("Trip")
                .document(firebaseUser
                        .getUid()).collection("myTrip")
                .whereEqualTo("saved", true)
                .orderBy("uploaded", Query.Direction.DESCENDING);
        FirestoreRecyclerOptions<TripModel> allusersaved = new FirestoreRecyclerOptions.Builder<TripModel>().setQuery(query,TripModel.class).build();
        tripAdapter =  new TripAdapter(allusersaved, saved_page.this, getResources(), firebaseUser, firebaseFirestore);
        triprv = findViewById(R.id.trip_rv);
        linearLayoutManager =  new LinearLayoutManager(saved_page.this);
        triprv.setLayoutManager(linearLayoutManager);
        triprv.setAdapter(tripAdapter);
        tripAdapter.notifyDataSetChanged();
//        int itemCount = tripAdapter.getItemCount();
//        if (itemCount > 0){
//            triprv.setVisibility(View.VISIBLE);
//            pages_null.setVisibility(View.GONE);
//        }else{
//            triprv.setVisibility(View.GONE);
//            pages_null.setVisibility(View.VISIBLE);
//        }

    }

    public void back(View view) {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }


    @Override
    public void onStart() {
        super.onStart();
        tripAdapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (tripAdapter != null){
            tripAdapter.stopListening();
        }
    }
}