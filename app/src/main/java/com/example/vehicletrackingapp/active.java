package com.example.vehicletrackingapp;

import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.HashMap;
import java.util.Map;

import Adapter.TripAdapter;
import AddOn.DateFormatterToAlphabet;
import Data.TripModel;

public class active extends Fragment {
    RecyclerView triprv;
    LinearLayoutManager linearLayoutManager;
    DateFormatterToAlphabet dateconvert;
    FirebaseUser firebaseUser;
    FirebaseFirestore firebaseFirestore;

    TripAdapter tripAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_active, container, false);
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        firebaseFirestore = FirebaseFirestore.getInstance();
        dateconvert = new DateFormatterToAlphabet();
        Query query = firebaseFirestore.collection("Trip")
                .document(firebaseUser
                .getUid()).collection("myTrip")
                .whereEqualTo("visited", false)
                .orderBy("uploaded", Query.Direction.DESCENDING);
        FirestoreRecyclerOptions<TripModel> alluseractive = new FirestoreRecyclerOptions.Builder<TripModel>().setQuery(query,TripModel.class).build();
        tripAdapter =  new TripAdapter(alluseractive, getContext(), getResources(), firebaseUser, firebaseFirestore);
        triprv = view.findViewById(R.id.trip_rv);
        linearLayoutManager =  new LinearLayoutManager(getContext());
        triprv.setLayoutManager(linearLayoutManager);
        triprv.setAdapter(tripAdapter);
        tripAdapter.notifyDataSetChanged();
        return view;
    }
    public class ActiveViewHolder extends RecyclerView.ViewHolder
    {
        private TextView dates;
        private TextView title_notes;
        private TextView text_location;
        private ImageView plan_list_image;
        private ImageButton saved_btn;
        public ActiveViewHolder(@NonNull View itemView) {
            super(itemView);
            dates = itemView.findViewById(R.id.dates);
            title_notes = itemView.findViewById(R.id.title_notes);
            text_location = itemView.findViewById(R.id.text_location);
            plan_list_image = itemView.findViewById(R.id.plan_list_img);
            saved_btn = itemView.findViewById(R.id.btn_saved);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        tripAdapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (tripAdapter != null){
            tripAdapter.stopListening();
        }
    }
}