package com.example.vehicletrackingapp;

import static androidx.constraintlayout.helper.widget.MotionEffect.TAG;

import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import Adapter.ImageAdapter;
import Adapter.RecomendAdapter;
import Adapter.TripAdapter;
import AddOn.DateFormatterToAlphabet;
import Data.RecomendData;
import Data.TripModel;

public class Recomend extends Fragment {
    RecyclerView triprv;
    LinearLayoutManager linearLayoutManager;
    DateFormatterToAlphabet dateconvert;
    FirebaseUser firebaseUser;
    FirebaseFirestore firebaseFirestore;
    View empty_content;
    TripAdapter tripAdapter;
    RecomendAdapter recomendAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recomend, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.recycler);
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        firebaseFirestore = FirebaseFirestore.getInstance();
        dateconvert = new DateFormatterToAlphabet();
        empty_content = view.findViewById(R.id.empty_content);
        Query recomend = firebaseFirestore.collection("Recomend");
        FirestoreRecyclerOptions<RecomendData> recomendOptions = new FirestoreRecyclerOptions.Builder<RecomendData>().setQuery(recomend,RecomendData.class).build();
        recomendAdapter = new RecomendAdapter(recomendOptions, getContext(), getResources(), firebaseUser, firebaseFirestore);
        recyclerView.setAdapter(recomendAdapter);
//        ArrayList<String> arrayList = new ArrayList<>();
//
//        arrayList.add("https://images.unsplash.com/photo-1692528131755-d4e366b2adf0?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHwzNXx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=500&q=60");
//        arrayList.add("https://images.unsplash.com/photo-1692862582645-3b6fd47b7513?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw0MXx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=500&q=60");
//        arrayList.add("https://images.unsplash.com/photo-1692584927805-d4096552a5ba?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw0Nnx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=500&q=60");
//        arrayList.add("https://images.unsplash.com/photo-1692854236272-cc49076a2629?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw1MXx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=500&q=60");
//        arrayList.add("https://images.unsplash.com/photo-1681207751526-a091f2c6a538?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHwyODF8fHxlbnwwfHx8fHw%3D&auto=format&fit=crop&w=500&q=60");
//        arrayList.add("https://images.unsplash.com/photo-1692610365998-c628604f5d9f?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHwyODZ8fHxlbnwwfHx8fHw%3D&auto=format&fit=crop&w=500&q=60");
//
//        ImageAdapter adapter = new ImageAdapter(getContext()
//                , arrayList);
//        adapter.setOnItemClickListener(new ImageAdapter.OnItemClickListener() {
//            @Override
//            public void onClick(ImageView imageView, String path) {
//                Intent intent = new Intent(requireContext(), ImageViewsActivity.class);
//                intent.putExtra("image", path);
//
//                // Start the activity with a shared element transition
//                startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(requireActivity(), imageView, "image").toBundle());
//            }
//        });
//        recyclerView.setAdapter(adapter);
        Date currentDate = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        String formattedDate = formatter.format(currentDate);

        Query query = firebaseFirestore.collection("Trip")
                .document(firebaseUser
                        .getUid()).collection("myTrip").whereEqualTo("visited", false)
                .whereEqualTo("date", formattedDate)
                .orderBy("uploaded", Query.Direction.DESCENDING);
        FirestoreRecyclerOptions<TripModel> allusertrip = new FirestoreRecyclerOptions.Builder<TripModel>().setQuery(query,TripModel.class).build();
        tripAdapter = new TripAdapter(allusertrip, getContext(), getResources(), firebaseUser, firebaseFirestore);
        triprv = view.findViewById(R.id.trip_rv);
        linearLayoutManager =  new LinearLayoutManager(getContext());
        triprv.setLayoutManager(linearLayoutManager);
        triprv.setAdapter(tripAdapter);
        tripAdapter.notifyDataSetChanged();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        tripAdapter.startListening();
        recomendAdapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (tripAdapter != null){
            tripAdapter.stopListening();
            recomendAdapter.stopListening();
        }
    }
}