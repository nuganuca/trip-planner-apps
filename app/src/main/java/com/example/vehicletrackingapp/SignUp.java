package com.example.vehicletrackingapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

import AddOn.LoadingDialog;

public class SignUp extends AppCompatActivity {
    EditText name, email, password, confirm_password;
    Button register;
    private FirebaseAuth mAuth;
    LoadingDialog loadingDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        name = findViewById(R.id.txt_nama);
        email = findViewById(R.id.txt_email);
        password = findViewById(R.id.txt_password);
        confirm_password = findViewById(R.id.txt_confirmPass);
        register = findViewById(R.id.register_button);
        loadingDialog = new LoadingDialog(this);
        TextView tvLogin= findViewById(R.id.tv_SignIn_now);
//        meisahkan kata Don't have an account? Register now
        String text = "Already have account? Sign in now";
        SpannableString spannableString = new SpannableString(text);
        ForegroundColorSpan colorSpan = new ForegroundColorSpan(getResources().getColor(R.color.primary_color));
        //aksi saat kata register di pencet
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                startActivity(new Intent(SignUp.this, SignIn.class));
                finish();
            }
        };
        //menyembunyikan action bar

        int start = text.indexOf("Sign in now");
        int end = start + "Sign in now".length();
        spannableString.setSpan(colorSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(clickableSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvLogin.setText(spannableString);
        tvLogin.setMovementMethod(LinkMovementMethod.getInstance());
        register.setOnClickListener(v -> {
            if (name.getText().length() > 0 && email.getText().length() > 0 && password.getText().length() > 0 && confirm_password.getText().length() > 0){
                if (password.getText().toString().equalsIgnoreCase(confirm_password.getText().toString())){
                    register(name.getText().toString(), email.getText().toString(), password.getText().toString());
                    loadingDialog.startLoadingDialog();
                }else{
                    password.setError("password tidak sama");
                    confirm_password.setError("password tidak sama");
                }
            }else{
                if(name.getText().length() == 0){
                    name.setError("isi name terlebih dahulu");
                }else if(email.getText().length() == 0){
                    email.setError("isi email anda terlebih dahulu");
                }else if (password.getText().length() == 0){
                    password.setError("isi password anda terlebih dahulu");
                }else if(confirm_password.getText().length() == 0){
                    confirm_password.setError("isi confirmasi password anda");
                }
            }
        });
        mAuth = FirebaseAuth.getInstance();
    }
    private void register(String nama, String email, String password){
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener( new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful() && task.getResult() != null){
                    FirebaseUser firebaseUser = task.getResult().getUser();
                    if (firebaseUser != null){
                        UserProfileChangeRequest request = new UserProfileChangeRequest.Builder()
                                .setDisplayName(nama)
                                .build();
                        firebaseUser.updateProfile(request).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                reload();
                            }
                        });
                    }else{
                        Toast.makeText(getApplicationContext(), "Register Gagal", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(), task.getException().getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                }
                loadingDialog.dismissDialog();
            }
        });
    }
    public void reload(){
        startActivity(new Intent(SignUp.this, SignIn.class));
        finish();
    }
}