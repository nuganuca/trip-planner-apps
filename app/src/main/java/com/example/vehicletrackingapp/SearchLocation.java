package com.example.vehicletrackingapp;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import Adapter.LocationSearchAdapter;
import Data.LocationSearchData;
import Fetch.FetchLocationSearchDataTask;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class SearchLocation extends DialogFragment {
    private RecyclerView recyclerView;
    private LocationSearchAdapter locationSearchAdapter;
    private EditText searchForm;

    private static final long DEBOUNCE_TIMEOUT = 500; // Waktu jeda (dalam milidetik)
    private final Handler handler = new Handler();
    private Runnable searchRunnable;
    @Override
    public void onStart() {
        super.onStart();
        if (getDialog() != null) {
            int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.95);
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            getDialog().getWindow().setLayout(width, height);
        }
    }

    public interface OnDataReceivedListener {
        void onDataReceived(LocationSearchData data);
    }

    private OnDataReceivedListener onDataReceivedListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_location, container, false);
        searchForm = view.findViewById(R.id.txt_search);
        recyclerView = view.findViewById(R.id.search_location_rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        locationSearchAdapter = new LocationSearchAdapter(new ArrayList<>());
        recyclerView.setAdapter(locationSearchAdapter);
        searchForm.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                handler.removeCallbacks(searchRunnable);
                searchRunnable = () -> onSearchLocation(s.toString());
                handler.postDelayed(searchRunnable, DEBOUNCE_TIMEOUT);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
//        searchForm.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if (actionId == EditorInfo.IME_ACTION_DONE) {
//                    String query = searchForm.getText().toString();
//                    onSearchLocation(query); // Gantilah dengan aksi yang sesuai
//                    return true;
//                }
//                return false;
//            }
//        });
        locationSearchAdapter.setOnItemClickListener(new LocationSearchAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(LocationSearchData locationData) {
                // Tanggapi klik item di sini

//                String data = locationData.getTitle() + "\n" +
//                        locationData.getAddressLabel() + "\n" +
//                        "Latitude: " + locationData.getLatitude() + "\n" +
//                        "Longitude: " + locationData.getLongitude();
                sendDataToMainActivity(locationData);
                dismiss();
            }
        });

        return view;
    }
    public void onSearchLocation(String queryString){
        String apiKey = "jdj__mbulQRO8zJlzvDEChhQ1vFdMoEN1r-NqxJmX0c";
        String url = "https://geocode.search.hereapi.com/v1/geocode?q="+queryString+"&apiKey=" + apiKey;
        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnected()){
            if(queryString.length() > 0) {
                new FetchLocationSearchDataTask(locationSearchAdapter).execute(url);;
            }
        }else {
            Toast.makeText(getContext(), "tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
        }
    }
    // Set listener untuk MainActivity
    public void setOnDataReceivedListener(OnDataReceivedListener listener) {
        this.onDataReceivedListener = listener;
    }

    // Panggil metode ini untuk mengirim data ke MainActivity
    private void sendDataToMainActivity(LocationSearchData data) {
        if (onDataReceivedListener != null) {
            onDataReceivedListener.onDataReceived(data);
        }
    }
}