package com.example.vehicletrackingapp;

import static androidx.constraintlayout.helper.widget.MotionEffect.TAG;

import android.content.Intent;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.Objects;

import Adapter.TripAdapter;
import Data.TripModel;


public class Home extends Fragment {
    CardView saved, search;
    TextInputLayout searchLayout;
    TextView title_page;
    EditText searchTxt;
    ImageView searchImg;
    RecyclerView searchrv;
    LinearLayout searchContent, homeContent;
    int currentItem = 0;
    String tabName = "Recomended";
    View loading_elemet;
    boolean isSearch = false;
    private static final long DEBOUNCE_TIMEOUT = 500; // Waktu jeda (dalam milidetik)
    private final Handler handler = new Handler();
    private Runnable searchRunnable;

    FirebaseUser firebaseUser;
    FirebaseFirestore firebaseFirestore;

    LinearLayoutManager linearLayoutManager;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        firebaseFirestore = FirebaseFirestore.getInstance();
        searchrv = view.findViewById(R.id.rvsearch);
        search = view.findViewById(R.id.searchCard);
        searchImg = view.findViewById(R.id.searchImgv);
        searchLayout = view.findViewById(R.id.layout_search);
        searchTxt = view.findViewById(R.id.txt_search);
        title_page = view.findViewById(R.id.title_page);
        searchContent = view.findViewById(R.id.searchContent);
        homeContent = view.findViewById(R.id.homeCotent);
        loading_elemet = view.findViewById(R.id.loading_elemet);
        TabLayout tabLayout = view.findViewById(R.id.tab);
        tabLayout.addTab(tabLayout.newTab().setText("recomend"));
        tabLayout.addTab(tabLayout.newTab().setText("active"));
        tabLayout.addTab(tabLayout.newTab().setText("past"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        //setting view pager dengan manggunakan pagerAdapter
        ViewPager viewPager = view.findViewById(R.id.pager);
        PagerAdapter adapter = new Adapter.PagerAdapter(getActivity().getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        //untuk
        viewPager.addOnPageChangeListener(new
                TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.setCurrentItem(currentItem);
        title_page.setText("My Trips | " + tabName);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                currentItem = viewPager.getCurrentItem();
                switch (currentItem){
                    case 0:
                        tabName = "Recomended";
                        break;
                    case 1:
                        tabName = "Active";
                        break;
                    case 2:
                        tabName = "Past";
                        break;
                    default:
                        tabName = "saved";
                        break;
                }
                title_page.setText("My Trips | " + tabName);
                cancelSearch();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        // intent ke halaman saved
        saved = view.findViewById(R.id.saved);
        saved.setOnClickListener(v -> {
            startActivity(new Intent(getContext(), saved_page.class));
        });

        search.setOnClickListener(v->{
            if (isSearch){
                cancelSearch();
            }else {
                setSearch();
            }
        });

        //search
        searchTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                loading_elemet.setVisibility(View.VISIBLE);
                searchrv.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                handler.removeCallbacks(searchRunnable);
                searchRunnable = () -> onSearchTrip(s.toString());
                handler.postDelayed(searchRunnable, DEBOUNCE_TIMEOUT);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return view;
    }
    private void setSearch(){
        isSearch = true;
        searchImg.setImageDrawable(getResources().getDrawable(R.drawable.remove));
        searchLayout.setVisibility(View.VISIBLE);
        title_page.setVisibility(View.GONE);
        searchContent.setVisibility(View.VISIBLE);
    }
    private void cancelSearch(){
        isSearch = false;
        searchImg.setImageDrawable(getResources().getDrawable(R.drawable.baseline_search_24));
        searchLayout.setVisibility(View.GONE);
        title_page.setVisibility(View.VISIBLE);
        searchrv.setVisibility(View.GONE);
        searchContent.setVisibility(View.GONE);
    }
    private void onSearchTrip(String q){
        if (!Objects.equals(q, "")){
            // Convert q ke lowercase untuk pencarian tanpa memperhatikan case
            Query query;
            String queryText = q.toLowerCase();
            switch (currentItem){
                case 0:
                    query = firebaseFirestore.collection("Trip")
                            .document(firebaseUser.getUid())
                            .collection("myTrip")
                            .whereGreaterThanOrEqualTo("title_lowercase",queryText)
                            .orderBy("uploaded", Query.Direction.DESCENDING);
                    break;
                case 1:
                    query = firebaseFirestore.collection("Trip")
                            .document(firebaseUser.getUid())
                            .collection("myTrip")
                            .whereEqualTo("visited", false)
                            .whereGreaterThanOrEqualTo("title_lowercase",queryText)
                            .orderBy("uploaded", Query.Direction.DESCENDING);
                    break;
                case 2:
                    query = firebaseFirestore.collection("Trip")
                            .document(firebaseUser.getUid())
                            .collection("myTrip")
                            .whereEqualTo("visited", true)
                            .whereGreaterThanOrEqualTo("title_lowercase",queryText)
                            .orderBy("uploaded", Query.Direction.DESCENDING);
                    break;
                default:
                    query = firebaseFirestore.collection("Trip")
                            .document(firebaseUser.getUid())
                            .collection("myTrip")
                            .whereGreaterThanOrEqualTo("title_lowercase",queryText)
                            .orderBy("uploaded", Query.Direction.ASCENDING);
                    break;
            }

            FirestoreRecyclerOptions<TripModel> allusertrip = new FirestoreRecyclerOptions.Builder<TripModel>().setQuery(query, TripModel.class).build();
            TripAdapter tripAdapter = new TripAdapter(allusertrip, getContext(), getResources(), firebaseUser, firebaseFirestore);
            linearLayoutManager = new LinearLayoutManager(getContext());
            searchrv.setLayoutManager(linearLayoutManager);
            searchrv.setAdapter(tripAdapter);
            tripAdapter.startListening();
            loading_elemet.setVisibility(View.GONE);
            searchrv.setVisibility(View.VISIBLE);
        } else {
            loading_elemet.setVisibility(View.VISIBLE);
            searchrv.setVisibility(View.GONE);
        }
    }
}