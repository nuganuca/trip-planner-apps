package com.example.vehicletrackingapp;
import static androidx.constraintlayout.helper.widget.MotionEffect.TAG;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import AddOn.DateFormatterToAlphabet;
import AddOn.LoadingDialog;
import Data.DateModel;
import Data.LocationSearchData;
import Data.TripModel;
import Receiver.AlarmReceiver;

public class addNotes extends AppCompatActivity implements SearchLocation.OnDataReceivedListener{
    private static final int PICK_IMAGE_REQUEST = 1;
    private static final String CHANNEL_ID = "channel_id";
    TextView selectLocation, Showdate;
    ImageView addimg;
    Boolean ResetDate = false;
    Button addnotesbutton;
    ImageButton chooseimg;
    DateFormatterToAlphabet dateconvert;
    String startDate = "", endDate = "", addressLabel = "", title = "", todo = "", urlImage ="";
    Uri imgUri = null;
    double lat, lang;
    boolean saved = false, visited = false, alarm = false;
    private EditText Title, Todo;
    private LoadingDialog loadingDialog;

    FirebaseAuth firebaseAuth;
    FirebaseUser firebaseUser;
    FirebaseFirestore firebaseFirestore;
    FrameLayout maps_container;
    Animation topAnim, bottomAnim, fadeAnim;
    DateModel dateModel;
    Switch switchButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_notes);
        dateModel = new DateModel();
        addimg = findViewById(R.id.addImg);
        Showdate = findViewById(R.id.showDate);
        dateconvert = new DateFormatterToAlphabet();
        selectLocation = findViewById(R.id.selectLocation);
        maps_container = findViewById(R.id.maps_container);
        addnotesbutton = findViewById(R.id.addnotesbutton);
        switchButton = findViewById(R.id.switchButton);
        Title = findViewById(R.id.title);
        Todo = findViewById(R.id.todo);
        loadingDialog = new LoadingDialog(this);
        chooseimg = findViewById(R.id.chooseimg);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Calendar calendar = Calendar.getInstance();
                    TimePickerDialog timePickerDialog = new TimePickerDialog(
                            addNotes.this,
                            (timePicker, hour, minute) -> {
                                calendar.set(Calendar.HOUR_OF_DAY, hour);
                                calendar.set(Calendar.MINUTE, minute);
                                dateModel.setHour(hour);
                                dateModel.setMinute(minute);
                                alarm = true;
                            },
                            calendar.get(Calendar.HOUR_OF_DAY),
                            calendar.get(Calendar.MINUTE),
                            true
                    );
                    timePickerDialog.show();
                    timePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            alarm = false;
                            switchButton.setChecked(false);
                        }
                    });
                } else {
                    alarm = false;
                }
            }
        });
        fadeAnim = AnimationUtils.loadAnimation(this, R.anim.fadeanim);
        Showdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });
        selectLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSearchLocation();
            }
        });


        //using firebase

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        //add button action
        addnotesbutton.setOnClickListener(v->{
            title = Title.getText().toString();
            todo = Todo.getText().toString();
            Date date = new Date();
            if (alarm){
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, dateModel.getYear());
                calendar.set(Calendar.MONTH, dateModel.getMonth());
                calendar.set(Calendar.DAY_OF_MONTH, dateModel.getDay() - 1);
                calendar.set(Calendar.HOUR, dateModel.getHour());
                calendar.set(Calendar.MINUTE, dateModel.getMinute());
                setAlarm(calendar.getTimeInMillis());
            }
            if(title.length() > 0 && todo.length() > 0 && startDate.length() > 0 && addressLabel.length() > 0 && lat != 0 && lang != 0){
                loadingDialog.startLoadingDialog();
                if (imgUri != null){
                    uploadImageToFirebase(imgUri).thenAccept(imageUrl -> {
                        DocumentReference documentReference = firebaseFirestore.collection("Trip").document(firebaseUser.getUid()).collection("myTrip").document();
                        Map<String, Object> Trip = new HashMap<>();
                        Trip.put("title", title);
                        Trip.put("title_lowercase", title.toLowerCase());
                        Trip.put("todo", todo);
                        Trip.put("date", startDate);
                        Trip.put("second_date", endDate);
                        Trip.put("address_label", addressLabel);
                        Trip.put("lat", lat);
                        Trip.put("lang", lang);
                        Trip.put("saved", saved);
                        Trip.put("visited", visited);
                        Trip.put("uploaded", date);
                        Trip.put("imgUrl", imageUrl);
                        Trip.put("alarm", alarm);
                        documentReference.set(Trip).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void unused) {
                                loadingDialog.dismissDialog();
                                Toast.makeText(addNotes.this, "Data added successfully", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(addNotes.this, MainActivity.class));
                                finish();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                loadingDialog.dismissDialog();
                                Toast.makeText(addNotes.this, "Failed To Create Notes, err: "+e, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }).exceptionally(e -> {
                        loadingDialog.dismissDialog();
                        Toast.makeText(this, "gagal upload image", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                        return null;
                    });
                }else{
                    DocumentReference documentReference = firebaseFirestore.collection("Trip").document(firebaseUser.getUid()).collection("myTrip").document();
                    Map<String, Object> Trip = new HashMap<>();
                    Trip.put("title", title);
                    Trip.put("title_lowercase", title.toLowerCase());
                    Trip.put("todo", todo);
                    Trip.put("date", startDate);
                    Trip.put("second_date", endDate);
                    Trip.put("address_label", addressLabel);
                    Trip.put("lat", lat);
                    Trip.put("lang", lang);
                    Trip.put("saved", saved);
                    Trip.put("visited", visited);
                    Trip.put("uploaded", date);
                    if (urlImage != ""){
                        Trip.put("imgUrl", Uri.parse(urlImage));
                    }else{
                        Trip.put("imgUrl", Uri.parse("https://firebasestorage.googleapis.com/v0/b/trip-planner-apps.appspot.com/o/images%2Fnoimagefound.jpg?alt=media&token=71ecfedb-bc1a-4a93-9a99-db84cfb6317a"));
                    }
                    Trip.put("alarm", alarm);
                    documentReference.set(Trip).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void unused) {
                            loadingDialog.dismissDialog();
                            Toast.makeText(addNotes.this, "Data added successfully", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(addNotes.this, MainActivity.class));
                            finish();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            loadingDialog.dismissDialog();
                            Toast.makeText(addNotes.this, "Failed To Create Notes, err: "+e, Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            }else{
                if (title.length() == 0){
                    Title.setError("Title harus diisi");
                }else if(todo.length() == 0){
                    Todo.setError("Todo tidak boleh kosong");
                }else if(addressLabel.length() == 0 || lat == 0 || lang == 0){
                    Toast.makeText(this, "pilih lokasi tujuan terlebih dahulu", Toast.LENGTH_SHORT).show();
                }else if(startDate.length() == 0){
                    Showdate.setError("pilih tanggal untuk pergi");
                }
            }
        });
        if (bundle != null){
            setrecomend(bundle);
        }
    }

    private void showDatePickerDialog() {
        // Dapatkan tanggal saat ini
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

        // Membuat DatePickerDialog
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                this,
                R.style.DatePickerDialogTheme, // Gaya kustom untuk dialog
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        // Lakukan sesuatu dengan tanggal yang dipilih
                        if (!ResetDate) {
                            startDate = day + "/" + (month + 1) + "/" + year;
                            dateModel.setYear(year);
                            dateModel.setMonth(month);
                            dateModel.setDay(day);
                            String formattedDate = dateconvert.DateFormatterToAlphabet(startDate);
                            Showdate.setText(formattedDate);
                            ResetDate = true;
                        } else {
                            // Ini adalah pemilihan kedua, gabungkan dengan tanggal pertama
                            endDate = day + "/" + (month + 1) + "/" + year;
                            Showdate.setText(dateconvert.DateFormatterToAlphabet(startDate) + " - " + dateconvert.DateFormatterToAlphabet(endDate));
                            ResetDate = false;
                        }
                    }
                },
                year, month, dayOfMonth);

        // Tampilkan DatePickerDialog
        datePickerDialog.show();
    }
    private void setAlarm(long timeInMillis) {
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        Intent intent = new Intent(this, AlarmReceiver.class);
        int requestCode = (int) System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // Atur alarm
        alarmManager.set(AlarmManager.RTC_WAKEUP, timeInMillis, pendingIntent);

        // Tampilkan notifikasi untuk memberitahu pengguna bahwa alarm telah diatur
        showNotification("Notifikasi perjalanan telah di set untuk: " + DateFormat.getDateTimeInstance().format(new Date(timeInMillis)));
    }

    private void showNotification(String message) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Memeriksa apakah versi Android 8.0 (Oreo) atau lebih tinggi
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "TripChannel", NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        // Intent untuk membuka aktivitas setelah notifikasi diklik
        Intent resultIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        // Membangun notifikasi
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.notification_logo)
                .setContentTitle("Trip Planner Apps")
                .setContentText(message)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        // Menampilkan notifikasi
        notificationManager.notify(0, builder.build());
    }
    private void showSearchLocation() {
        SearchLocation yourFragment = new SearchLocation();
        yourFragment.setOnDataReceivedListener(this);
        yourFragment.show(getSupportFragmentManager(), "dialog showed");
    }
    private CompletableFuture<String> uploadImageToFirebase(Uri imageUri) {
        CompletableFuture<String> future = new CompletableFuture<>();

        StorageReference storageRef = FirebaseStorage.getInstance().getReference();
        StorageReference imagesRef = storageRef.child("images/" + UUID.randomUUID().toString());

        imagesRef.putFile(imageUri)
                .addOnSuccessListener(taskSnapshot -> {
                    imagesRef.getDownloadUrl().addOnSuccessListener(uri -> {
                        String imageUrl = uri.toString();
                        future.complete(imageUrl);
                    });
                })
                .addOnFailureListener(e -> {
                    // Handle kegagalan upload
                    future.completeExceptionally(e);
                });
        return future;
    }
    @Override
    public void onDataReceived(LocationSearchData data) {
        // Tangani data yang diterima dari fragment di sini
        maps_container.setAnimation(fadeAnim);
        maps_container.setVisibility(View.VISIBLE);
        addressLabel = data.getAddressLabel();
        lat = data.getLatitude();
        lang = data.getLongitude();
        Toast.makeText(this, "Data received: " + data.getAddressLabel(), Toast.LENGTH_SHORT).show();
        selectLocation.setText(data.getAddressLabel());
        Bundle bundle = new Bundle();
        bundle.putDouble("latitude", data.getLatitude());
        bundle.putDouble("longitude", data.getLongitude());
        bundle.putString("address", data.getTitle());
        MapsFragment mapsFragment = new MapsFragment();
        mapsFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.maps_container, mapsFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            imgUri = data.getData();
            Glide.with(this)
                    .load(imgUri)
                    .into(addimg);
            addimg.setVisibility(View.VISIBLE);
        } else {
            // Handle case when user cancels image selection
            Toast.makeText(this, "Image selection canceled", Toast.LENGTH_SHORT).show();
        }
    }

    public void chooseImage(View view) {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }
    public void setrecomend(Bundle bundle){
        title = bundle.getString("title");
        lang = bundle.getDouble("lang");
        lat = bundle.getDouble("lat");
        urlImage = bundle.getString("imgUrl");
        addressLabel = bundle.getString("address_label");
        Title.setText(title);
        selectLocation.setText(addressLabel);
        if (urlImage  != ""){
            Glide.with(this)
                    .asBitmap()
                    .load(Uri.parse(urlImage))
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                            // Menggunakan image bitmap di sini
                            addimg.setImageBitmap(resource);
                        }
                    });
            addimg.setVisibility(View.VISIBLE);
        }
        maps_container.setVisibility(View.VISIBLE);
        Bundle bun = new Bundle();
        bun.putDouble("latitude", lat);
        bun.putDouble("longitude", lang);
        bun.putString("address", addressLabel);
        MapsFragment mapsFragment = new MapsFragment();
        mapsFragment.setArguments(bun);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.maps_container, mapsFragment)
                .addToBackStack(null)
                .commit();
    }

    public void back(View view) {
        startActivity(new Intent(addNotes.this, MainActivity.class));
        finish();
    }
}