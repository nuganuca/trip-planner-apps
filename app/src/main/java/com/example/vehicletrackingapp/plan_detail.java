package com.example.vehicletrackingapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import AddOn.DateFormatterToAlphabet;
import AddOn.LoadingDialog;
import Data.LocationSearchData;
import Data.TripModel;

public class plan_detail extends AppCompatActivity implements SearchLocation.OnDataReceivedListener{
    private static final int PICK_IMAGE_REQUEST = 1;
    TextView selectLocation, Showdate, btn_cancel, title_page;
    ImageView addimg;
    Boolean ResetDate = false;
    Button edittripbtn;
    ImageButton edit_btn, chooseimg;
    EditText Title, Todo;
    DateFormatterToAlphabet dateconvert;
    Uri new_imgUrl;
    String documentId;
    FrameLayout maps_container;
    TripModel model;
    boolean isEdit;
    FirebaseUser firebaseUser;
    FirebaseFirestore firebaseFirestore;
    LoadingDialog loadingDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan_detail);
        firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        model = new TripModel();
        addimg = findViewById(R.id.addImg);
        Showdate = findViewById(R.id.showDate);
        dateconvert = new DateFormatterToAlphabet();
        selectLocation = findViewById(R.id.selectLocation);
        maps_container = findViewById(R.id.maps_container);
        edit_btn = findViewById(R.id.button_edit);
        edittripbtn = findViewById(R.id.editnotesbutton);
        btn_cancel = findViewById(R.id.cancel_edit);
        title_page = findViewById(R.id.title_page);
        Title = findViewById(R.id.title);
        Todo = findViewById(R.id.todo);
        chooseimg = findViewById(R.id.chooseimg);
        loadingDialog = new LoadingDialog(this);
        edit_btn.setOnClickListener(v->{
            setup_edit();
        });
        btn_cancel.setOnClickListener(v->{
            setup_cancel();
        });
        setdetail();
        setup_cancel();
        Showdate.setOnClickListener(v->{
            showDatePickerDialog();
        });
        Showdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEdit){
                    showDatePickerDialog();
                }
            }
        });
        selectLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEdit){
                    showSearchLocation();
                }
            }
        });
        edittripbtn.setOnClickListener(v->{
            DocumentReference documentReference = firebaseFirestore.collection("Trip").document(firebaseUser.getUid()).collection("myTrip").document(documentId);
            Map<String, Object> updatedData = new HashMap<>();
            if (isEdit){
                model.setTitle(Title.getText().toString());
                model.setTodo(Todo.getText().toString());
                Date date = new Date();
                if(model.getTitle().length() > 0 && model.getTodo().length() > 0 && model.getDate().length() > 0 && model.getAddress_label().length() > 0 && model.getLang() != 0 && model.getLat() != 0){
                    loadingDialog.startLoadingDialog();
                    if (new_imgUrl != null){
                        uploadImageToFirebase(new_imgUrl).thenAccept(imageUrl -> {
                            updatedData.put("address_label", model.getAddress_label());
                            updatedData.put("date", model.getDate());
                            updatedData.put("second_date", model.getSecond_date());
                            updatedData.put("imgUrl", imageUrl);
                            updatedData.put("lang", model.getLang());
                            updatedData.put("lat", model.getLat());
                            updatedData.put("title", model.getTitle());
                            updatedData.put("todo", model.getTodo());
                            updatedData.put("uploaded", date);
                            updatedData.put("visited", model.isVisited());
                            updatedData.put("saved", model.isSaved());
                            documentReference.update(updatedData)
                                    .addOnSuccessListener(aVoid -> {
                                        loadingDialog.dismissDialog();
                                        Toast.makeText(this, "Data Edited Successfully", Toast.LENGTH_SHORT).show();
                                        startActivity(new Intent(plan_detail.this, MainActivity.class));
                                    })
                                    .addOnFailureListener(e -> {
                                        loadingDialog.dismissDialog();
                                        Toast.makeText(plan_detail.this, "Failed To Edit Trip", Toast.LENGTH_SHORT).show();
                                    });
                        }).exceptionally(e -> {
                            loadingDialog.dismissDialog();
                            Toast.makeText(this, "gagal upload image", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                            return null;
                        });
                    }else{
                        updatedData.put("address_label", model.getAddress_label());
                        updatedData.put("date", model.getDate());
                        updatedData.put("second_date", model.getSecond_date());
                        updatedData.put("imgUrl", model.getImgUrl());
                        updatedData.put("lang", model.getLang());
                        updatedData.put("lat", model.getLat());
                        updatedData.put("title", model.getTitle());
                        updatedData.put("todo", model.getTodo());
                        updatedData.put("uploaded", date);
                        updatedData.put("visited", model.isVisited());
                        updatedData.put("saved", model.isSaved());
                        updatedData.put("title_lowercase", model.getTitle_lowercase());
                        updatedData.put("alarm", model.isAlarm());
                        documentReference.update(updatedData)
                                .addOnSuccessListener(aVoid -> {
                                    loadingDialog.dismissDialog();
                                    Toast.makeText(this, "Data Edited Successfully", Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(plan_detail.this, MainActivity.class));
                                })
                                .addOnFailureListener(e -> {
                                    loadingDialog.dismissDialog();
                                    Toast.makeText(plan_detail.this, "Failed To Edit Trip", Toast.LENGTH_SHORT).show();
                                });
                    }
                }else{
                    if (model.getTitle().length() == 0){
                        Title.setError("Title harus diisi");
                    }else if(model.getTodo().length() == 0){
                        Todo.setError("Todo tidak boleh kosong");
                    }else if(model.getAddress_label().length() == 0 || model.getLat() == 0 || model.getLang() == 0){
                        Toast.makeText(this, "pilih lokasi tujuan terlebih dahulu", Toast.LENGTH_SHORT).show();
                    }else if(model.getDate().length() == 0){
                        Showdate.setError("pilih tanggal untuk pergi");
                    }
                }
            }else {
                loadingDialog.startLoadingDialog();
                updatedData.put("visited", true);
                documentReference.update(updatedData)
                        .addOnSuccessListener(aVoid -> {
                            loadingDialog.dismissDialog();
                            Toast.makeText(this, "Data Finished Successfully", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(plan_detail.this, MainActivity.class));
                        })
                        .addOnFailureListener(e -> {
                            loadingDialog.dismissDialog();
                            Toast.makeText(plan_detail.this, "Failed To Finished Trip", Toast.LENGTH_SHORT).show();
                        });
            }
        });
    }
    public void setup_cancel() {
        isEdit = false;
        chooseimg.setVisibility(View.GONE);
        btn_cancel.setVisibility(View.GONE);
        edit_btn.setVisibility(View.VISIBLE);
        title_page.setText("Detail Page");
        Title.setEnabled(false);
        Todo.setEnabled(false);
        edittripbtn.setText("Finish This Journey");
        setdetail();
    }

    public void setup_edit(){
        isEdit = true;
        chooseimg.setVisibility(View.VISIBLE);
        btn_cancel.setVisibility(View.VISIBLE);
        edit_btn.setVisibility(View.GONE);
        title_page.setText("Edit Page");
        Title.setEnabled(true);
        Todo.setEnabled(true);
        edittripbtn.setText("Save Edit");
    }
    private void showDatePickerDialog() {
        // Dapatkan tanggal saat ini
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

        // Membuat DatePickerDialog
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                this,
                R.style.DatePickerDialogTheme, // Gaya kustom untuk dialog
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        // Lakukan sesuatu dengan tanggal yang dipilih
                        if (!ResetDate) {
                            model.setDate(day + "/" + (month + 1) + "/" + year);
                            String formattedDate = dateconvert.DateFormatterToAlphabet(model.getDate());
                            Showdate.setText(formattedDate);
                            ResetDate = true;
                        } else {
                            // Ini adalah pemilihan kedua, gabungkan dengan tanggal pertama
                            model.setSecond_date(day + "/" + (month + 1) + "/" + year);
                            Showdate.setText(dateconvert.DateFormatterToAlphabet(model.getDate()) + " - " + dateconvert.DateFormatterToAlphabet(model.getSecond_date()));
                            ResetDate = false;
                        }
                    }
                },
                year, month, dayOfMonth);

        // Tampilkan DatePickerDialog
        if (isEdit){
            datePickerDialog.show();
        }
    }
    public void setdetail(){
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        model.setTitle(bundle.getString("title"));
        model.setTodo(bundle.getString("todo"));
        model.setLang(bundle.getDouble("lang"));
        model.setLat(bundle.getDouble("lat"));
        model.setDate(bundle.getString("date"));
        model.setSecond_date(bundle.getString("second_date"));
        model.setImgUrl(bundle.getString("imgUrl"));
        model.setAddress_label(bundle.getString("address_label"));
        model.setVisited(bundle.getBoolean("visited"));
        model.setSaved(bundle.getBoolean("saved"));
        model.setTitle_lowercase(bundle.getString("lowercase_title"));
        model.setAlarm(bundle.getBoolean("alarm"));
        documentId =  bundle.getString("documentId");
        Title.setText(model.getTitle());
        Todo.setText(model.getTodo());
        if (model.getSecond_date() != ""){
            Showdate.setText(model.getDate() + " - " + model.getSecond_date());
        }else{
            Showdate.setText(model.getDate());
        }
        selectLocation.setText(model.getAddress_label());

        if (model.getImgUrl()  != ""){
            Glide.with(plan_detail.this)
                    .asBitmap()
                    .load(model.getImgUrl())
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                            // Menggunakan image bitmap di sini
                            addimg.setImageBitmap(resource);
                        }
                    });
            addimg.setVisibility(View.VISIBLE);
        }
        maps_container.setVisibility(View.VISIBLE);
        selectLocation.setText(model.getAddress_label());
        Bundle bun = new Bundle();
         bun.putDouble("latitude", model.getLat());
        bun.putDouble("longitude", model.getLang());
        bun.putString("address", model.getAddress_label());
        MapsFragment mapsFragment = new MapsFragment();
        mapsFragment.setArguments(bun);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.maps_container, mapsFragment)
                .addToBackStack(null)
                .commit();
    }
    private void showSearchLocation() {
        SearchLocation yourFragment = new SearchLocation();
        yourFragment.setOnDataReceivedListener(this);
        yourFragment.show(getSupportFragmentManager(), "dialog showed");
    }
    private CompletableFuture<String> uploadImageToFirebase(Uri imageUri) {
        CompletableFuture<String> future = new CompletableFuture<>();

        StorageReference storageRef = FirebaseStorage.getInstance().getReference();
        StorageReference imagesRef = storageRef.child("images/" + UUID.randomUUID().toString());
        StorageReference old_storageRef = FirebaseStorage.getInstance().getReferenceFromUrl(model.getImgUrl());
        imagesRef.putFile(imageUri)
                .addOnSuccessListener(taskSnapshot -> {
                    imagesRef.getDownloadUrl().addOnSuccessListener(uri -> {
                        String imageUrl = uri.toString();
                        old_storageRef.delete();
                        future.complete(imageUrl);
                    });
                })
                .addOnFailureListener(e -> {
                    // Handle kegagalan upload
                    future.completeExceptionally(e);
                });
        return future;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            new_imgUrl = data.getData();
            Glide.with(this)
                    .load(new_imgUrl)
                    .into(addimg);
            addimg.setVisibility(View.VISIBLE);
        } else {
            // Handle case when user cancels image selection
            Toast.makeText(this, "Image selection canceled", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onDataReceived(LocationSearchData data) {
        // Tangani data yang diterima dari fragment di sini
        maps_container.setVisibility(View.VISIBLE);
        model.setAddress_label(data.getAddressLabel());
        model.setLat(data.getLatitude());
        model.setLang(data.getLongitude());
        Toast.makeText(this, "Data received: " + data.getAddressLabel(), Toast.LENGTH_SHORT).show();
        selectLocation.setText(data.getAddressLabel());
        Bundle bundle = new Bundle();
        bundle.putDouble("latitude", data.getLatitude());
        bundle.putDouble("longitude", data.getLongitude());
        bundle.putString("address", data.getTitle());
        MapsFragment mapsFragment = new MapsFragment();
        mapsFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.maps_container, mapsFragment)
                .addToBackStack(null)
                .commit();
    }

    public void chooseImage(View view) {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        if (isEdit){
            startActivityForResult(intent, PICK_IMAGE_REQUEST);
        }
    }

    public void back(View view) {
        startActivity(new Intent(plan_detail.this, MainActivity.class));
        finish();
    }
}