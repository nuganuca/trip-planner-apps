package com.example.vehicletrackingapp;

import static android.app.Activity.RESULT_OK;

import static androidx.constraintlayout.helper.widget.MotionEffect.TAG;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import AddOn.LoadingDialog;

public class account extends Fragment {
    private static final int PICK_IMAGE_REQUEST = 1;
    CardView logout;
    String userId;
    FirebaseAuth  firebaseAuth;
    FirebaseUser currentUser;
    FirebaseFirestore firebaseFirestore;
    CardView Account, about, CS, detailed_account;
    ImageView profileImage;
    EditText usr, usremail;
    TextView usn, email_top, username_top;
    ImageButton change_image;
    LoadingDialog loadingDialog;
    Uri imgUri = null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account, container, false);
        firebaseAuth = FirebaseAuth.getInstance();
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        firebaseFirestore = FirebaseFirestore.getInstance();
        logout = view.findViewById(R.id.logout);
        Account = view.findViewById(R.id.account);
        about = view.findViewById(R.id.about);
        CS = view.findViewById(R.id.CS);
        detailed_account = view.findViewById(R.id.detailed_account);
        usn =  view.findViewById(R.id.usn);
        firebaseAuth = FirebaseAuth.getInstance();
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        email_top = view.findViewById(R.id.email_top);
        username_top = view.findViewById(R.id.username_top);
        profileImage = view.findViewById(R.id.profile_img);
        change_image = view.findViewById(R.id.change_image);
        //set textview agar muncul nama user yang login
        userId = currentUser.getUid();
        usn.setText(currentUser.getDisplayName());
        loadingDialog = new LoadingDialog(getActivity());
        logout.setOnClickListener(v -> {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
            builder1.setMessage("are you sure to exit now?");
            builder1.setCancelable(true);
            builder1.setPositiveButton(
                    "Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
                            if (firebaseAuth.getCurrentUser() != null){
                                firebaseAuth.signOut();
                                startActivity(new Intent(getContext(), MainActivity.class));
                                getActivity().finish();
                            }
                        }
                    });
            builder1.setNegativeButton(
                    "No",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert11 = builder1.create();
            alert11.show();
        });
        String email = "nuganuca17@gmail.com";
        CS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse("mailto:" + email));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "dear, lazier");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "you can message your problem here");
                startActivity(Intent.createChooser(emailIntent, "Send feedback"));
            }
        });
        Account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detailed_account.getVisibility() == View.GONE){
                    detailed_account.setVisibility(View.VISIBLE);
                }else{
                    detailed_account.setVisibility(View.GONE);
                }
            }
        });
        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), about_page.class));
            }
        });
        change_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, PICK_IMAGE_REQUEST);
            }
        });
        usr = view.findViewById(R.id.usr);
        usremail = view.findViewById(R.id.usr_email);
        email_top.setText(currentUser.getEmail());
        username_top.setText(currentUser.getDisplayName());
        usr.setText(currentUser.getDisplayName());
        usremail.setText(currentUser.getEmail());
        setImgProfile();
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            imgUri = data.getData();
            Glide.with(this)
                    .load(imgUri)
                    .into(profileImage);
            uploadImageToFirebase(imgUri).thenAccept(imageUrl -> {
                loadingDialog.startLoadingDialog();
                DocumentReference documentReference = firebaseFirestore.collection("profile").document(currentUser.getUid());
                Map<String, Object> img = new HashMap<>();
                img.put("imageUrl", imageUrl);
                documentReference.set(img).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void unused) {
                        loadingDialog.dismissDialog();
                        Toast.makeText(getContext(), "Data added successfully", Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        loadingDialog.dismissDialog();
                        Toast.makeText(getActivity(), "Failed To Create Notes, err: "+e, Toast.LENGTH_SHORT).show();
                    }
                });
            }).exceptionally(e -> {
                loadingDialog.dismissDialog();
                Toast.makeText(getContext(), "gagal upload image", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
                return null;
            });
        } else {
            // Handle case when user cancels image selection
            Toast.makeText(getContext(), "Image selection canceled", Toast.LENGTH_SHORT).show();
        }
    }
    private CompletableFuture<String> uploadImageToFirebase(Uri imageUri) {
        CompletableFuture<String> future = new CompletableFuture<>();

        StorageReference storageRef = FirebaseStorage.getInstance().getReference();
        StorageReference imagesRef = storageRef.child("profile/" + UUID.randomUUID().toString());

        imagesRef.putFile(imageUri)
                .addOnSuccessListener(taskSnapshot -> {
                    imagesRef.getDownloadUrl().addOnSuccessListener(uri -> {
                        String imageUrl = uri.toString();
                        future.complete(imageUrl);
                    });
                })
                .addOnFailureListener(e -> {
                    // Handle kegagalan upload
                    future.completeExceptionally(e);
                });
        return future;
    }
    private void setImgProfile() {
        if (currentUser != null) {
            String uid = currentUser.getUid();
            DocumentReference documentReference = firebaseFirestore.collection("profile").document(uid);

            documentReference.get().addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    Log.d(TAG, "nugroho: " + document);
                    String imgUrl = document.getString("imageUrl");
                    if ( imgUrl != null) {
                        Log.d(TAG, "nugroho: " +  imgUrl);
                        if (imgUri == null){
                            Glide.with(getActivity())
                                    .asBitmap()
                                    .load(Uri.parse(imgUrl))
                                    .into(new SimpleTarget<Bitmap>() {
                                        @Override
                                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                            // Use the bitmap image here
                                            profileImage.setImageBitmap(resource);
                                        }
                                    });
                        }else{
                            Glide.with(getActivity())
                                    .asBitmap()
                                    .load(imgUri)
                                    .into(new SimpleTarget<Bitmap>() {
                                        @Override
                                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                            // Use the bitmap image here
                                            profileImage.setImageBitmap(resource);
                                        }
                                    });
                        }
                    } else {
                        // Document not found
                    }
                } else {
                    // Error getting the document
                }
            });
        } else {
            // User not logged in, handle accordingly
        }
    }

    @Override
    public void onStart() {
        super.onStart();

    }
}