
package com.example.vehicletrackingapp;

import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.HashMap;
import java.util.Map;

import Adapter.TripAdapter;
import AddOn.DateFormatterToAlphabet;
import Data.TripModel;


public class History extends Fragment {
    RecyclerView pastrv;
    LinearLayoutManager linearLayoutManager;
    DateFormatterToAlphabet dateconvert;
    FirebaseUser firebaseUser;
    FirebaseFirestore firebaseFirestore;

    TripAdapter pastAdapter;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_history, container, false);
        pastrv = view.findViewById(R.id.pastrv);
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        firebaseFirestore = FirebaseFirestore.getInstance();
        dateconvert = new DateFormatterToAlphabet();

        Query query = firebaseFirestore.collection("Trip")
                .document(firebaseUser.getUid())
                .collection("myTrip")
                .whereEqualTo("visited", true)
                .orderBy("uploaded", Query.Direction.DESCENDING);

        FirestoreRecyclerOptions<TripModel> alluserpast = new FirestoreRecyclerOptions.Builder<TripModel>().setQuery(query,TripModel.class).build();
        pastAdapter =  new TripAdapter(alluserpast, getContext(), getResources(), firebaseUser, firebaseFirestore);
        linearLayoutManager = new LinearLayoutManager(getContext());
        pastrv.setLayoutManager(linearLayoutManager);
        pastrv.setAdapter(pastAdapter);
        pastAdapter.notifyDataSetChanged();
        return view;

    }
    @Override
    public void onStart() {
        super.onStart();
        pastAdapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (pastAdapter != null){
            pastAdapter.stopListening();
        }
    }
}