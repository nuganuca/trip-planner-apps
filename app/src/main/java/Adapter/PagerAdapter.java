package Adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.vehicletrackingapp.History;
import com.example.vehicletrackingapp.Recomend;
import com.example.vehicletrackingapp.active;

public class PagerAdapter extends FragmentStatePagerAdapter {
    int numTab;
    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.numTab = NumOfTabs;
    }

    @NonNull
    @Override
    public androidx.fragment.app.Fragment getItem(int position) {
        switch (position){
            case 0:
                return new Recomend();
            case 1:
                return new active();
            case 2:
                return new History();
            default:
                return null;
        }
    }

    @Override
    public int getCount(){
        return numTab;
    }
}
