package Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.example.vehicletrackingapp.R;
import com.example.vehicletrackingapp.addNotes;
import com.example.vehicletrackingapp.plan_detail;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

import Data.RecomendData;
import Data.TripModel;

public class RecomendAdapter extends FirestoreRecyclerAdapter<RecomendData, RecomendAdapter.ViewHolder> {
    private Context context;
    private Resources resources;
    FirebaseUser firebaseUser;
    FirebaseFirestore firebaseFirestore;

    public RecomendAdapter(@NonNull FirestoreRecyclerOptions<RecomendData> options, Context context, Resources resources, FirebaseUser firebaseUser, FirebaseFirestore firebaseFirestore) {
        super(options);
        this.context = context;
        this.resources = resources;
        this.firebaseUser = firebaseUser;
        this.firebaseFirestore = firebaseFirestore;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.image_list_item, parent, false);
        return new ViewHolder(view);
    }


    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull RecomendData model) {
        Glide.with(context)
                .asBitmap()
                .load(model.getImgUrl())
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        // Menggunakan image bitmap di sini
                        holder.imageView.setImageBitmap(resource);
                    }
                });
        holder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(context, addNotes.class);
            intent.putExtra("title", model.getTitle());
            intent.putExtra("imgUrl", model.getImgUrl());
            intent.putExtra("lat", model.getLat());
            intent.putExtra("lang", model.getLang());
            intent.putExtra("address_label", model.getAddress_label());
            context.startActivity(intent);
        });
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.list_item_image);
        }
    }

}
