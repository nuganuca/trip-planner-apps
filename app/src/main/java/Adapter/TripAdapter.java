package Adapter;

import static androidx.core.content.ContextCompat.startActivity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.example.vehicletrackingapp.R;
import com.example.vehicletrackingapp.Recomend;
import com.example.vehicletrackingapp.plan_detail;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.HashMap;
import java.util.Map;

import AddOn.DateFormatterToAlphabet;
import Data.TripModel;

public class TripAdapter extends FirestoreRecyclerAdapter<TripModel, TripAdapter.TripViewHolder> {
    private Context context;
    private Resources resources;
    FirebaseUser firebaseUser;
    FirebaseFirestore firebaseFirestore;
    public TripAdapter(@NonNull FirestoreRecyclerOptions<TripModel> options, Context context, Resources resources, FirebaseUser firebaseUser, FirebaseFirestore firebaseFirestore) {
        super(options);
        this.context = context;
        this.resources = resources;
        this.firebaseUser = firebaseUser;
        this.firebaseFirestore = firebaseFirestore;
    }

    @Override
    protected void onBindViewHolder(@NonNull TripViewHolder holder, int position, @NonNull TripModel model) {
        DateFormatterToAlphabet dateconvert = new DateFormatterToAlphabet();
        holder.title_notes.setText(model.getTitle());
        holder.text_location.setText(model.getAddress_label());
        String date;
        if (model.getSecond_date() != ""){
            date = dateconvert.DateFormatterToAlphabet(model.getDate()) + " - " + dateconvert.DateFormatterToAlphabet(model.getSecond_date());
        }else {
            date = dateconvert.DateFormatterToAlphabet(model.getDate());
        }
        holder.dates.setText(date);
        TripModel updatedTrip = new TripModel();
        updatedTrip.setSaved(model.isSaved());
        updatedTrip.setVisited(model.isVisited());
        DocumentSnapshot snapshot = getSnapshots().getSnapshot(holder.getAdapterPosition());
        String documentId = snapshot.getId();
        if (model.isVisited()){
            holder.saved_btn.setVisibility(View.GONE);
        }
        if (model.isVisited()){
            holder.saved_btn.setVisibility(View.GONE);
            holder.delete_btn.setVisibility(View.VISIBLE);
            holder.restore_btn.setVisibility(View.VISIBLE);
        }
        holder.restore_btn.setOnClickListener(v->{
            TripModel newsaved = new TripModel();
            DocumentReference documentReference = firebaseFirestore.collection("Trip").document(firebaseUser.getUid()).collection("myTrip").document(documentId);
            if (model.isVisited()){
                newsaved.setSaved(false);
                holder.restore_btn.setVisibility(View.GONE);
            }
            Map<String, Object> updatedData = new HashMap<>();
            updatedData.put("visited", newsaved.isVisited());
            documentReference.update(updatedData)
                    .addOnSuccessListener(aVoid -> {
                        Toast.makeText(context, "Data restored successfully", Toast.LENGTH_SHORT).show();
                    })
                    .addOnFailureListener(e -> {
                        Toast.makeText(context, "Failed to restore data", Toast.LENGTH_SHORT).show();
                    });
        });
        holder.delete_btn.setOnClickListener(v->{
            DocumentReference documentReference = firebaseFirestore.collection("Trip").document(firebaseUser.getUid()).collection("myTrip").document(documentId);
            documentReference.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void unused) {
                    Toast.makeText(context, "Data Deleted Successfully", Toast.LENGTH_SHORT).show();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(context, "Failed to delete data", Toast.LENGTH_SHORT).show();
                }
            });
        });
        holder.saved_btn.setOnClickListener(v -> {
            DocumentReference documentReference = firebaseFirestore.collection("Trip").document(firebaseUser.getUid()).collection("myTrip").document(documentId);
            if (updatedTrip.isSaved()){
                updatedTrip.setSaved(false);
                holder.saved_btn.setImageDrawable(resources.getDrawable(R.drawable.saved));
            }else {
                updatedTrip.setSaved(true);
            }
            Map<String, Object> updatedData = new HashMap<>();
            updatedData.put("saved", updatedTrip.isSaved());
            documentReference.update(updatedData)
                    .addOnSuccessListener(aVoid -> {
                        if (updatedTrip.isSaved()){
                            Toast.makeText(context, "Trip Added to Bookmark", Toast.LENGTH_SHORT).show();
                            holder.saved_btn.setImageDrawable(resources.getDrawable(R.drawable.saved_filled));
                        }else{
                            Toast.makeText(context, "Trip Removed From Bookmark", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(e -> {
                        Toast.makeText(context, "gagal menyimpan Trip", Toast.LENGTH_SHORT).show();
                    });
        });
        if (model.isSaved()){
            holder.saved_btn.setImageDrawable(resources.getDrawable(R.drawable.saved_filled));
        }else{
            holder.saved_btn.setImageDrawable(resources.getDrawable(R.drawable.saved));
        }
        Glide.with(context)
                .asBitmap()
                .load(model.getImgUrl())
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        // Menggunakan image bitmap di sini
                        holder.plan_list_image.setImageBitmap(resource);
                    }
                });
        holder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(context, plan_detail.class);
            intent.putExtra("title", model.getTitle());
            intent.putExtra("todo", model.getTodo());
            intent.putExtra("date", model.getDate());
            intent.putExtra("second_date", model.getSecond_date());
            intent.putExtra("imgUrl", model.getImgUrl());
            intent.putExtra("lat", model.getLat());
            intent.putExtra("lang", model.getLang());
            intent.putExtra("address_label", model.getAddress_label());
            intent.putExtra("saved", model.isSaved());
            intent.putExtra("visited", model.isVisited());
            intent.putExtra("documentId", documentId);
            intent.putExtra("lowercase_title", model.getTitle_lowercase());
            intent.putExtra("alarm", model.isAlarm());
            // Start detail activity
            context.startActivity(intent);
        });
    }

    @NonNull
    @Override
    public TripViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.plan_list_item, parent, false);
        return new TripViewHolder(view);
    }

    public class TripViewHolder extends RecyclerView.ViewHolder
    {
        private TextView dates;
        private TextView title_notes;
        private TextView text_location;
        private ImageView plan_list_image;
        private ImageButton saved_btn, restore_btn, delete_btn;
        public TripViewHolder(@NonNull View itemView) {
            super(itemView);
            dates = itemView.findViewById(R.id.dates);
            title_notes = itemView.findViewById(R.id.title_notes);
            text_location = itemView.findViewById(R.id.text_location);
            plan_list_image = itemView.findViewById(R.id.plan_list_img);
            saved_btn = itemView.findViewById(R.id.btn_saved);
            restore_btn = itemView.findViewById(R.id.btn_restore);
            delete_btn = itemView.findViewById(R.id.btn_delete);
        }
    }
    public int getItemCount() {
        return super.getItemCount();
    }
}
