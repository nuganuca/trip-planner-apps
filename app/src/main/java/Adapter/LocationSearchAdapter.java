// LocationSearchAdapter.java

package Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vehicletrackingapp.R;

import java.util.List;

import Data.LocationSearchData;

public class LocationSearchAdapter extends RecyclerView.Adapter<LocationSearchAdapter.LocationViewHolder> {
    private List<LocationSearchData> locationList;
    private OnItemClickListener onItemClickListener;

    public LocationSearchAdapter(List<LocationSearchData> locationList) {
        this.locationList = locationList;
    }

    @NonNull
    @Override
    public LocationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search, parent, false);
        return new LocationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LocationViewHolder holder, int position) {
        LocationSearchData locationData = locationList.get(position);
        holder.titleTextView.setText(locationData.getTitle());
        holder.addressLabelTextView.setText(locationData.getAddressLabel());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(locationData);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return locationList.size();
    }

    public void setData(List<LocationSearchData> newData) {
        // Clear existing data
        locationList.clear();
        // Add new data
        locationList.addAll(newData);
        // Notify RecyclerView that the data has changed
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }

    public static class LocationViewHolder extends RecyclerView.ViewHolder {
        TextView titleTextView;
        TextView addressLabelTextView;

        public LocationViewHolder(@NonNull View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.destination_name);
            addressLabelTextView = itemView.findViewById(R.id.destination_addres);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(LocationSearchData locationData);
    }
}
